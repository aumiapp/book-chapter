Interview themes and topics: 
============================

## Eric: 

- personal involvement with project
- MacKay Centre School pilot project
- must serve needs of end users
- functionality: break down barriers of ability
- functionality: collective play
- functionality: therapeutic potentialities
- role: conduit between practitioners and development team
- features: tracking dot
- challenges to research at school
- history: McGill housed development
- split with iOS version
- design challenges: compatibility with older/Windows/low cost systems
- design challenges: ease of setup and use
- balance simplicity with functionality for the community
- keeping AUMI free
- Leaf as expert user
- design for accessibility
- developers over-complexifying the app
- challenge assumptions about the end user experience
- addressing cross- and different-ability groups, multi-modailty
- technical problems: dot
- issues: environmental variables in setup
- feature (future): reinventing AUMI
- feature (future): physical controllers
- development (current & future): developing in Max
- need for more resources/financial support
- design challenge: dependability, robustness
- contributing to the social good
- feature (current): modularity (v4)
- current development to refine existing version
- future development to design other kinds of interfaces

## Thomas: 

- musical involvement with Pauline
- involvement with Deep Listening
- involvement in project - belief in contributing to the social good
- development (past): create v3 Windows version
- balance simplicity with functionality for the community
- understanding the needs of end users
- development (v4): modularity provides more flexibility
- development (v4): improved user interface
- development (v4): code cleanup
- attribute: flexibility and adaptability
- Leaf's guidance of the project
- design for accessibility - understand end user
- project in re-evaluation phase now
- Pauline's passing
- iPad version moving in different direction
- feature (future): bring iPad version functionalities to desktop
- development (current & future): developing in Max
- development (future): web/JS version
- development (future): physical computing
- guidelines: keeping AUMI free and openly accessible
- balance simplicity and open access with development and new features
- need for more resources and funding
- project is labor of love
- collaborative model carrying into own work
- role: liaison between practitioners and technical team
- role: assist with dev continuity
- benefit of AUMI team meetups
- tracking dot behavior - understanding user needs

## Ian:

- motivation: funding
- consistency across many versions
- development (v3): visual representations, lines and dot
- development (v3): small improvements to stable app, user friendly
- work was secondary research, part time after primary phd research
- phsycial controller prototypes
- balance between research & innovation, and maintaining a simple stable app
- challenge of moving from prototyping to a robust real world application
- application is a tool that becomes an instrument through the practitioners
- Leaf as expert user
- development must serve needs of end users
- understanding users' specific needs
- free reign to explore in academic research settings
- ensure productive outcomes from academic research
- AUMI development as a creative endeavor
- lack of feedback from users via website/download mechanism
- need for larger conversation around AUMI 
- feature (future): collect quantitative data for analysis
- challenges to research in schools
- need for more resources, larger scope
- using AUMI in different ways - guitar/pop songs
- AUMI as assemblage (with samples, individual interpretations, etc.)
- making AUMI open to interact with other systems
- limitations of modular approach - fixed mappings, input to output
- limitations of development by non-developers in Max
- development (future): web version

## Ivan: 

- balancing development support and new ideas
- redesign from ground up (v4)
- getting existing users to adopt a new version
- limitations of Max - managing complexity
- limitations of Max - many developers 
- the need for modularity
- feature (v3 & v4): presets
- understanding unique requirements of end users
- using user-centered design
- color palates - minor details that are important usability issues
- need for a developer guide
- need for more resources and active researchers
- rewarding to see real-world deployment
- personalization of AUMI by the practitioners
- engagement of AUMI community
- AUMI research as a labour of love
- hope for the long term continuity of the project
- how to address different user needs, different modalities
- physical interfaces
- interface with existing mobility equipment (wheelchairs, walkers, playgrounds)
- sense of AUMI community
- need for better funding
- keeping AUMI free and operational on older systems
- need for overall vision and growing the goals of the project
- viability of development in Max
- Max allowing for developer continuity 
- constraints of Max: complex solutions to simple problems (scaling different camera aspect ratios)
- constraints of Max: security measures, signing apps
- limitation of small development team - need to continually test and update
- ending support for older systems/users
- future development: improve usability for non-technical users
- AUMI as group of applications
- future development: embedded computing --> interactive sound objects
- future work: better understand requirements for designing assistive technology
- future work: extend beyond music interaction

# Chapter Outline: 

1. Introduction
    1. The AUMI project and a brief history of its development at the IDMIL, especially starting with Ian. 
        1. Mention of Mackay Centre School pilot project
    2. Timeline of developers and releases/milestones
2. Version 3: improvements and developemnt
3. Version 4: redesign from the ground up
4. Current state and thoughts for future development 
5. Themes on the AUMI project - visions and high level ideas
6. Conclusion and thoughts for the future

