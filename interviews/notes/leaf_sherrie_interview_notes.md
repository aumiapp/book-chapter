Video interview of Leaf Miller

by Sherrie Tucker, 2017-12-09

Leaf is demonstrating several older versions of AUMI that she has on her laptop. 

### Beta version March 2009 (Zevin): 

- screen is very small and can't enlarge
- adjust brightness
- built by Zevin
- Different modes: 
    - Keyboard mode
    - Quarter Screen Percussion
    - Relative Movement Percussion
    - Split Screen Percussion
-  (still) only 3 scales - maj/min/blues
-  only MIDI sounds in keyboard mode
    -  percussion modes have their own percussion sounds
-  volume and record didn't work
-  can change size/color of dot, but can change size of box
- "very scaled down"

### Beta version December 2010

- bigger screen 

### Unidentified 2012 version (Ian developed)

- more adjustability
- quick startup
- change size and color of guides and tracking dot
- introduction of video fullscreen
- this was basically the version until v4

### v3.1 2014 (Ian)

- Dot returns to tracking area if moved off screen
- Tracking improvements maybe?
- 'Optional tracking settings' introduced