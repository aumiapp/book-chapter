# Interview 3: Ian Hattwick

Wednesday 2019-09-1?
Skype

## When did you get involved with AUMI and what did you work on?

Started working on AUMI around 2012. Looking for funding. Eric had list of requested improvements from AUMI users. Ian added features... 

AUMI when Ian started (v3) wasn't much more complicated from the original version. Leaf has laptop with many old versions. 

Biggest contributions were changing visual representations of lines and dot on screen. Keyboard mode, make own scale and other options; change velocity of note based on vertical positioning (removed in v4), lines can be vertical or horizontal; bugfixes, etc.

Other things that had been talked about in terms of therapeutic goals, though largely not implemented. Most development was towards making the application more user friendly. 

Added presets. 

Worked on it for 1/5 to 2 years. Not a heavy commitment, but worked on it among other projects. Then Ivan came and did full rewrite v4. 

V4 is now out, has been out for 2 years +/- and is still functionally very much the same. And underlying technology is the same too (Max and cv.jit objects). 

Also worked on external input devices, though didn't come to fruition. Made some video demos to show to Eric: for students with differing abilities: a physical "joystick" that could be grabbed/nudged... Working with 3D printing mechanical assemblies of different shapes and forms that fit around a custom design sensor equipped PCB. 

ME: theme for current/future research, custom controllers and physical input devices to address different adaptive needs. Ivan's v4 is modular with the intent of allowing for different input devices (both virtual/software and hardware). I mention the Ultrahaptics/AUMI demo for CHI as another example. 

{audio clip: 10:30 or so to 11:20}

One of the tensions that came up was the interplay between, on one hand, being researchers, thinking about novel and interesting ways to solve problems (of interaction, amongst other things), and on the other hand, AUMI is very user focused and the application needs it to do what it needs to do... simply. So making transition from new idea to something that can be implemented in real world application is hard. From design to use is very hard. 

AUMI is built around Max, which has been implemented already, so it is already user-ready... Tried and true. 

"Leaf is AUMI" - AUMI as an application is so simple. It's not designed to draw you in, learning curve, rewards, designing interactive experiences. People will see what it does very quickly, but may not be inspired to engage with it.

---> link this with Eric's segment about new user perception of the tracking dot being broken. DESIGNING interactions, how to "sell" the app.

The thing that really motivates people to engage with it are the concepts of its use withing these social musical settings, and specifically the people who are leading it. 

AUMI symposium 201? got to see Leaf workshop with kids at MCS, that was impressive. In this way, AUMI is a tool for the practitioners, and doesn't need to do more than what it does. 

Tension between 2 motivations for people working on AUMI. On one hand, people who develop technologies for people who have specific needs, and people projecting on to them what they think the users need... (link with Eric's segment on this). On the other hand, university setting, funded research, and there is some free reign for people to explore. If this was a commercial project this wouldn't be okay. This is a bigger project within our lab, how to we ensure something productive comes out of it - research, training and experience, or something that is actually usable for an end user. 

Eric doesn't want to be a project manager, and realizes that on some level this is a creative endeavor. Give people motivation to think outside the box, and also reign people in as well. 

ME: not a heavy emphasis on dissemination of findings, or rigorous research methods or evaluation and testing. It's a free place to explore. 

Nothing wrong with that approach... BUT.. wish for there to be a little more concrete outputs. Maybe this book will be one? More publishing. 

As a developer, to produce a deliverable and give it to the users, query them, get data back, and go through that iterative cycle to produce something usable. It's a lot of work. 

## What about feedback from the users when you developed it? 

Never saw results of the feedback was collected. Memory was that it was more of a roadblock when the application was downloaded. (check Thomas, I think he says something similar). ...a dissuasion. Had to fill it out before you could even download it. 

ME: There isn't data to track who is currently using the app. iOS app is doing well, more portable. But also it is more complex. 

Seeing it in use, at MCS and ICASP (?) symposium, reinforced the narrative that it needs to be simple. Easy for a conversation with users to become a laundry list of wishes, features, bugfixes. 

Another level of conversation to have around AUMI, potentially much deeper. About assistive technologies more generally, how to understand what will really be more valuable, and how to prioritize that in the UI and development. 

Felt not satisfied about the conversations around AUMI.. Devolved and somewhat mundane lists of things. And missing a larger conversation. Not terribly deep or interesting. 

{audio clip: around 26:00}

What would be interesting to get for feedback from people, that addresses the UX? What would be interesting to document about the user experience? Interested in documenting quantitative data: how much students achieve their tasks? Working with project at a rehabilitation hospital for children to develop interactive applications, collecting quantitative data. 

ME: iOS has quantitative data collection... For desktop - have discussed the same, but what data to collect, and who is looking at it? A question of general project scope and management. 

Again, it's easy to do development in a lab, but when bringing it to other communities the work is exponential. It becomes  an impediment if its not your full time research job. 

If Ian was Eric, he would be very frustrated.. that level of funding is not there to allow this level of research. Had the sense that Eric was always a bit frustrated with this aspect. Always the lowest research priority. (with Ian, with me, etc.)

Marcelo and feedback, not much (me) or explicitly, not to prioritize it (Ian). So for us at IDMIL, advanced AUMI research has explicitly been deprioritized. A regret/missed opportunity for us. 

Realities of doing a PhD, and research, and funding... 

## General thoughts

Memories on the project were examples of use with Leaf, and MCS classroom sessions. Leaf has such a spirit. 

Keiko Thomas' work and her students working at MCS, developing new uses of the technology - loading guitar chords onto the app and playing pop songs... making it their own. Incumbent on the individual (practitioner) to make it interesting. 

ME: Jesse Stewart, another example of taking the AUMI and using in innovative ways. 

Instrument building is part of the practice - people making assemblages of different kinds of things, AUMI as one tool, so are the samples, etc. common practices in digital media. 

With AUMI it is about the creativity and commitment of using the tool. Nice that AUMI is open ended to support that. 

{audio clip: }
how to design a system that can be friendly enough to be open to other systems. Think of the violin, but really what you want to make is a bow. but the bow doesn't really mean much without something to bow. But you can bow anything not just a violin, and in that way a bow is an incredibly flexible instrument! So maybe in that way AUMI is somewhere between a bow and a violin... 

ME: back to Ivan's modular system. Will AUMI continue to be a single app or a suite of apps with different use cases, or a more theoretical research project to address some of these higher level inquiries around design for accessibility? 
--> thematic chapter concept: 3 levels of AUMI: 

1. Develop and maintain a simple CV app, where the practitioner makes the interaction
2. Expand the app with new technologies, interaction modalities
3. Examine accessible music making practice and research around it from the ground up. What is the vision for AUMI?

limits of the existing modular structure is that it is fixed: input maps to output

Max: is that the right environment to develop in? Need a "real" developer. We (IDMIL) are not actually software developers. Henry is, and it shows with the iOS app. Raises the tension more broadly within the music tech community between people who are trained engineers and those who are trained musicians, artists, etc. and are constrained by the limitations of these platforms (eg., Max). 

ME: interest in JS/browser version. P5.js and tone.js and we're done.. "It's at Arduino level." 

Ian, now that it's mentioned, yes AUMI should totally be a web app! 

ME: bring iOS and desktop apps closer together. Core functionality is similar, but everything else is different. 