# Interview 2: Ivan Franco

Thursday 2019-09-12
CIRMMT A822

## What is/was your involvement with the AUMI project and IDMIL? 

First heard of AUMI through Ian (previous developer), looking for a new dev to continue work. During PhD at McGill/IDMIL. Need development support and open to new ideas. Could look at what was there and suggest modifications and contribute to the vision of what AUMI is. 

Involvement was full redesign of the application, v4. Started by analyzing v3 and what were the weaknesses. What improvements could we bring? Led to the redesign and release of version 4. 

ME: mention Leaf has several older versions of AUMI. 

{audio clip 4:00 - 5:30} challenges with releasing a new version

Many challenges and areas for improvement in v3. The biggest challenge is how to get user to adopt a new version - always expectations regarding what the application did before, along with new interfaces and new functionalities to think about. For a long-time user this is a challenge. Big redesigns that are hard for previous users. The goal is to make people understand where we are going with the new version, and why we are doing the modifications we are doing, and essentially to train them, to evangelize them. Especially with a big team like AUMI. 

## What was important to change?

Previous version had the concept of what AUMI was supposed to be, developed with Max. 

A big challenge with Max, with developer turnover especially, is how to manage complexity within a patch/project. Space to create a version that was more flexible and more open to continuous improvement. ...a modular approach. 

Core functionality: piano, movement tracking (computer vision), but all very fixed. Not easy for a developer to create a new module for different input, or different synthesis. 

{audio clip: 8:20 - 10:00} rationale for new version. 

Each child has their own requirements, own way of seeing the world, from interaction modalities to response to sound and music. 

Idea for the new app is to create new modules. A basis for growth. 

ME:  How can a team continue to develop an app with constant developer turnover? 
What is user experience? Having presets... adapt for each child, special settings for individuals. With that, small usability issues that we tried to solve. 

{audio clip... 12?? see below}

"an easy and relaxed way of doing user-centered design". was able to bring ideas and developments to the team, and get feedback. For example: color palates, which was a big discussion, and perhaps a seeming minor detail, but a big usability issue. So... little things that could make or break a good application. 

So that was a key factor for success, working closely with a team, and the ability to combine that with own experience in user interaction. 

ME: need for funding... user guides and videos! I wrote a user guide.. Need for feedback 

Need to create documentation for the developers. In addition to a user guide, a developer guide. Technological groundwork is there. 

## What are some most important/meaningful aspects of AUMI that you have experienced? 

Seeing the deployment, visits at MCS and seeing children engaging with the application is the reward you get from being involved with the application. From single notes and sound textures to rock music... The new version responds better to those requirements. 

--> cna link above to Ian's recollection with MCS guitar chords... 

make it personal! 

engagement of the AUMI community... (see Thomas' comments) Doing the AUMI meetings in Montreal. 

It feels like a work of passion <--- mentioned before! ...that was thoroughly enjoyed. 

AUMI is a long term project, that seems to have the strength to continue. Even if it takes a new form. Valid goal: how do we bring music to kids/children with differing abilities? 

There are many existing and new ideas that we have, and space to develop them. For example, while AUMI is based on computer vision, how to engage users with different interaction modalities? Physical interfaces? 

How to adapt wheelchair standard switches, for example. Go to something that may be more rewarding for some of those kids. 

A lot of ideas are still in the air.. constant look for innovation. Observe the kids, come back with things they can use, is fantastic. That sense of community is one of the hardest things to build, and with AUMI it came organically. 

ME: introduced AUMI at OHMI conference, was one of the longest running projects... longevity! 

ME: Pauline's passing... dormant for first 6 months, then more activity now.. 

A good goal would be to try to find better funding for it now. Better resources would help if move forward further. 

Not only AUMI, or music interaction, but the cost of interactive systems for children with disabilities. Which are very expensive and now accessible to many. Systems for non-verbal children communicate, for example. Proprietary equipment.

It's good to have some sort of control for products that are put on the market, but open source/free versions, even lover quality versions, would be great. 

{good audio clip: 24:00 - ... }

ME: for example, AUMI eye tracking systems. 

There is still a lot of space to grow what the goals of something like AUMI is. OVerall vision, where is the project going? 

Hope to see growth, not about an application, but how can technology be used to address the unique needs of these users, to make their lives better? 

## How have specific technologies helped or hindered the ongoing development of AUMI? 

Ongoing debate about using Max... But, stuck with it because it is a system that a lot of people are familiar with. Leave Max behind? No, keep it for now.. Can be difficult in terms of reviewing past work and managing complexity, but it allows for continuity. 

Example of Max requiring a complex solution to a seemingly simple problem. Different webcams: 4x3 or 16x9. Shocking to present skewed image to a user. This is a huge UX and also accessibility issue, there may be psychological implications to kids seeing their own likeness skewed. So, to be able to always present the camera view always in the correct aspect, is really important. So, diversity of hardware is important. 

Security measures, signing applications that allow for easy download and distribution. (ME: now for Windows too!) This is problematic... And there are more security layers coming. 

Creates a strain on the developers. Especially small developers like AUMI. Must test a lot, but also update continuously. This is a problem with many or all digital systems today. And at some point you many need to leave some users behind (on older systems). 

This can be more important in schools, working on older computers, older operating systems. This has put a lot of pressure on the AUMI development. 

{audio clip: 36:00 - }

Hope for the future to make it a bit smoother.. Make is easier for non-technical users. 


Any application is a tradeoff between functionality and complexity. Before we had less things, and it just worked. Now we have more things, and it takes more to make it work. 

Would it be better for AUMI to be a group of applications? Could be easier for development.. 

{audio clip: 36:50 - } heavy metal guitar.. 

ME: developing physical interfaces. The more I develop, the more I hack the modularity of the system that V4 is designed around. 

Embedded computing: ability to put the sound processing inside objects. A physical object that can create sound. Could think about an interactive object, with sounds, haptic and visual feedback. 

Maybe more demanding, need to develop hardware, not just hardware. 

ME: Vanier/Viger.. (?) mobile interaction/music workshops - opportunity to think of these things for accessibility. Could have talks and allow for future adaption for disability development. 

WOuld like to see - us understanding the requirements for these sorts of devices, working with users like MCS. And to explore other types of technologies (music or interactive), because there is so much to be done. 