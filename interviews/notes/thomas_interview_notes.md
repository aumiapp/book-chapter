Interview 4: Thomas Ciufo
=========================

Wednesday 2019-09-23  
Skype

Thomas is at Mt. Holyoke College, Massachusetts, USA

## What is your involvement with the AUMI project, when did you start and how did that come about?

Probably longest currently running on the tech team after Eric, started
in 2013.

First Deep Listening Conference hosted at EMPAC at RPI. Had know and
worked with Pauline before that but not via AUMI. Met Leaf, Henry and
others, and got to see AUMI in use, talks, etc. Saw that AUMI was
meaningful and important. Had been mostly bootstrapped and didn't have a
lot of resources behind it.

Thomas offered to help out. Pauline recommended occupying an 'in-between
space', to bridge between coders/developers (with some turn-over) and
researchers, practitioners, and users. Serve as an ad-hoc technical
liaison. In earlier times there was greater developer turnover, so
Thomas would help to bring new devs up to speed, and help provide
continuity. Also can help translate technical aspects and limitations to
practitioners, and vice versa.

## What was your connection with Pauline and Deep Listening Institute before AUMI?

Encountered Pauline's work early on, did a Deep Listening retreat in
grad school. Continued working with Pauline in a number of capacities.
Pauline was interested in Thomas' instrument building in grad research,
and was a guest reader for dissertation (2004). Stayed in touch and did
some collaborative performances together.

AUMI wasn't/isn't central to Thomas' own research and work, but he felt
it was important to pitch in and help in any way he could.

(seems to be a theme... interest in wanting to be involved and
contribute, based on fundamental belief in the project)

When Thomas started: v3 out, maybe Chuck was the developer? Mac version
stable but no Windows version - that was first priority. Even though
Macs are common around here (N.America, academia), large parts of the
world run on Windows, making AUMI unaccessible.

Would get new programmers frequently (via McGill/Eric/IDMIL/CIRMMT).. so
turnover was quick.

3.1 was major, with Windows support and other improvements. Then v4 with
Ivan, to re-envision the project more broadly.

## What were some of the important milestones that came during v3, before v4 was released?

V3 already existed when Thomas joined the project. Windows support was
major, and this is a lingering issue even today..

{audio clip start: 8:04 - 8:50} - balancing simplicity with ongoing
development and addition of new features and functionality

Project is enigmatic, because it has some high aspirations, but in the
end it doesn't have to do that much. It doesn't have to be an expert
performance system, it has to be accessible.

v4 was a big step in terms of program design.

## Thoughts on new version?

Not an expert user. Trying to understand how individuals are using it
and what are their needs.

Important points: Decoupling interaction mode from instrument gives more
flexibility; back-end code cleanup; user interface streamlining, robust
presets.

{audio clip: 12:15 - 14:04}

Project wasn't meant to be a musical instrument in a traditional sense.
Pauling called it musical instrumentS. Wider notion of musicality - can
be many different things to different people. Flexible and adaptable.

--> add Ian's interview clip here: The instrument is nothing without
the person using it!

{audio clip: 15:35 - 17:15} - Leaf's guidance of the project, and the
experience of the parents..

be sensitive to who is actually using the AUMI - many devs are
instrument designers, but we need to design for them, not us. (can link
to Eric's audio)

## Where the project is, and where is it going?

Me: ideas on development.. there have been many different prototypes
built for the AUMI - simple eye tracking systems, haptic tracking; web
version?; physical interfaces; accommodation for users with different
abilities, use different modalities

Thomas: re-evaluation phase right now.

Pauline's passing, project has moved forward, but with different feel
and impetus. The iPad version has moved in a different direction - open
question about the relationship between iPad and desktop versions. Add
some iPad functionality into desktop version?

{audio clip: 22:10 - 26:33}

Max has it's own opportunities and limitations. Most readily availabl
and widely used platform that is familiar to art/science devs who are
typically doing the development. Haven't had a pure CS dev team. Our of
necessity... resisting moving to a more proprietary language because it
may preclude future developers from being able to work on it.

But, web audio has come a long way... There are opportunities to
explore. If it could offer easier accessibility, should be ope for
discussion.

Physical computing and hardware is interesting and holds a lot of
promise. Project has been resistant to this, maybe mostly due to cost -
keeping the AUMI accessible. Could be pursued..

Look at medical technology, with very expensive proprietary technology.
Thomas will only keep working on the project as long as it's free.

Need to balance development with accessibility and access.

--> could link Ivan's interview about physical devices and embedded
computing

me: need for more resources/funding...

The project has been a labour of love for many, and survived without
much funding. Ad-hoc research group, and dev turnover.

## Parts of the project that have carried over into your own work or research?

Maybe in non-obvious ways.. The direct correlation is minimal, but: this
is an incredible team, sense of collaboration and teamwork is strong,
and this carries into own project management style.

{audio clip: 34:?? - 36:16}

Pauline's influence - guidance with a light touch - the collaborative
model.

Understanding of how bodies function in the world

## Feedback from practitioners

One of Thomas' main goals was to help with dev continuity and liaison
between the practitioners and the technical team.

Therapists and practitioners in the field can report directly back to
the team (via website/email). Also full meet ups with the entire team -
practitioners and technical team.

Need for more in-person team interactions

{audio: 43:15} --> tracking dot: "Time-out, return to center" becomes
an important concept.
