# Interview 1: Eric Lewis

Wednesday 2019-09-11  
CIRMMT A822

## What is your involvement with the AUMI? 

Start with International Institute for Critical Studies in Improvisation ([IICSI][iicsi]) and previously Improvisation Community and Social Practice ([ICASP][icasp]): 

- Canadian based research teams funded by SSHRC based in Guelph with McGill as the 'second' site
- site coordinator
- One of the projects at McGill was providing the tech support/development for the AUMI project

AUMI developed out of research axis of ICASP "Improvisation Gender and the Body", included several researchers who are still part of the AUMI group and Pauline Oliveros. Thought the research was exciting and high-quality. Eric asked to become a member of the research team "because I'd really love to work with you all".

Feminist theory - there were increased interest in notions of intersectional identities and disabilities, and disability studies. The reearch team begain working with the AUMI. 

The AUMI was originally conceived by Pauline Oliveros and designed by people working closely with her (Zane and Zevin). Since then, Eric has overseen graduate students out of CIRMMT [and/or IDMIL] to work on refining the software. Also established a laboratory at the MacKay Centre School (MSC) in Montreal (school for children with physical disabilities, part of the English school board). THis was done partially through the enthusiastic response of the principle after seeing an AUMI demo. 

[Keiko Shikako-Thomas][keiko], professor from the School of Physical and Occupational Therapy was also doing research at the MSC, then Eric and Keiko together ran programs using AUMI at the school, and using feedback from children, teachers, caregivers, therapists, to refine AUMI. 

{audio clip, start at 5:30 }
"The nature of AUMI as a device, it's very crucial that what it does is a function of what the people that are going to use it want it to be."

2 complementary yet contradictory functionalities of AUMI: 

1. Originally was primarily a tool to break down barriers of ability so that all people/children can enter into collective creative play around sound. A device to facilitate community building. 
2. Serious therapeutical potentialities, and we have been working to refine AUMI, and thinking about ways to use it therapeutically. 

These 2 can go hand in hand, but not always. Eric's primary role has been as a conduit between the people using the device, and the developers who are working to refine the device; to translate the desires of the users into technical requirements. 

Required learning some technical foundations of AUMI; what's possible, what's easy, what to save for future versions, etc.

MCS example - changeable dot

{ 9:15 cut out "of jesus" } 

{ end clip: 11:11. Or could cut earlier, after immediate MCS example}
Also, I think Ian has a recollection of this as well, can put the two together. 

## What is the current status of MCS? 

In hiatus. MCS moved into a new building last year (2018), principle retired, and Keiko and Eric are working to collect and publish the data from the research that has already been done there. Don't need to just "continue on", but would like to come back with more refined research, new approaches, collect new data, etc., to make it more useful. Also always contingent on funding, there are added expenses working with children, and especially with children with disabilities (added staff, equipment). It takes a lot of planning to do these types of interventions in school systems.

Topic: Challenges to doing this research within a school... including their own ethics regulations. Takes a lot of planning, infrastructure, funding, time.

## When did development move from RPI to McGill? 

There were developers before Ian, and there continue to be independent developers. Aaron Krajinski, Chuck Bronson worked on it before Ian. Eric's best guess is that it's been housed at McGill for the last 10 years.

Bifurcation between desktop and iOS versions, with separate development teams for each (iOS is all Henry, TBH...)

## What sort of changes have you seen the development go through? What are some milestones that you saw that were meaningful in not only the development of technology, but also matched or reflected its use in the field through MCS or other places?


{ also a good audio clip.. around 18 min? }
Motion to music technology is now quite old, so in that regard AUMI tech is nothing new. So the technical challenges are not at the "how do you do it" level. Instead at the level of:

- how to do it on the kinds of devices that get donated to centers for kids with disabilities like old Windows 97 machines, how to do it without fancy motion tracking cameras (use built-in webcams)
- how to do it so we don't need a technician there to help set up and run it. (Thinking of the end user)

BUT, The ultimate end user, and we still have yet to solve this problem, is the kid in a wheelchair. Can't power device on, can't use it without help. But we have worked on refining, is to build in as many functionalities as the community you're working with would like to have: like changing color, [presets - ME]. But do that while keeping it simple enough so the teacher or caregiver can still use the application easily. 

Thought a lot about AUMI being free, which is crucial. What would you get with a paid app? A phone number... support! 

So what we have done is balance adding features and functionality, while keeping it simple and easy for the end user to operate. 

Leaf: knows AUMI inside and out, and has been using all versions since the beginning (see Sherrie's interview) and knows all the nuances of each version, so providing a new version, even a superior version, will necessitate adapting to various things...

{ audio clip: 21:27 }
graduate students thinking of new features for the AUMI. Fatally flawed in many ways... a lot of them focused on turned it into a cool, creative instrument. Thinking like musicians, able-bodied musicians. 

"we can't put ourselves in the subjectivity of the end users, which is why we need them to power the changes and the development to the degree that's possible. It's very hard. That's why we have to work very close with caretakers and parents and teachers." 

{end clip 23:50}

Examples of working with children.. Challenging our own assumptions about the end user experience.. We don't know, and need to rely on the caretakers, etc. 
{ audio clip 24:00 }

- child triggered by dog barking sounds
- Greece 2 day session

{ could include this whole clip through 28:00 }

## AUMI is assumably filling a need for an instrument that can be used by people of ALL (dis)abilities. Fundamental design has remained the same (CV-based interaction to trigger sounds). How have we collectively considered or negotiated all different types of abilities beyond just movement disabilities, etc.?

Simply by dividing it up into different investigations. Ie., Jesse Stewart uses the device in many different ways with cross-ability groups. MCS has worked primarily with physical disabilities. Divide up these concerns and see what can be transferrable. 

Another way is to build it into a curriculum (as at MCS). Use it outside of just the specific therapy sessions, and to be used by all the kids and put them on equal footing. 

The main obstacle that Eric has faced in "selling" AUMI is the perception that the tracking doesn't work. (The dot flies away..) People will think it doesn't work. But we know it kind of doesn't matter.. the cursor is a sort of stand-in for what is really going on. From a development side, we need to spend more time on that even though it's a (variably) false problem. 

Another problem with AUMI is there is a huge differential in the ability of people who use it in their practice to really use it well. For instance, Leaf knows it very well - quick setup, lighting, background, everything works and is optimized. Others will struggle with little things that make it not work well. So, there is a lot of variability between the users, which effects the ability to "sell" the device to people that would use it. Would want Leaf to demonstrate it.. the effect will be different. 

ME: the environmental variables are hard to get past. We spent a lot of time with presets to speed up the setup, but still something as simple as a classroom change, or wearing different clothes, can require everything to be set up different. 

Eric: The design question would be: if you were to create a new AUMI, starting from zero tomorrow, would you be writing it in Max? Would we be using the built-in camera? Or whatever the questions would be, but there could be some substantial differences in how a brand new AUMI might work.. 

ME: Ian was looking into some physical controllers and lighting? 

{also a good audio clip, around 40:00 - the ideal device}

Eric: Designing something with lights (motion to light). The gold standard: would love to have enough grant money.. The ideal device would be to work with a small groups or individuals, and design bespoke interfaces for them. Maybe the software framework would be the same but be able to quickly prototype different physical interfaces. 

The problem of course is time and resources to develop, and ensure that these types of devices would be robust, easy to set up and operate. 

Eric: these are not difficult problems to solve in the real world, given a budget and 8 graduate students. The social good would be pretty immediate.

ME: A great contribution of Ivan's work was to plan out a modular system that could be built upon, to accommodate different interaction modalities and route to different types of outputs (instruments). 

{ could include a video of the CHI demo... }

{ audio clip - view for the future.. 44:00 }
It would be amazing if labs like IDMIL (shout out Marcelo), if every project thought for some amount of time about who could use the interface, and what could be done to allow for adaptation to an accessible interface, ie., t-stick shaped into a U that could be mounted on a wheelchair.. Early planning in the design of new interfaces. Could have a lot of potential adaptive interfaces out there. 

ME: Talking about the Viger project - designing some new interfaces for a mobile music interaction workshop.. 

Eric: "Every instrument is an adaptive instrument. The problem is too many of them historically are designed to be adapted to a normalized sense of what a body is."

## What do you see as both immediate and long term directions for the AUMI, not only just the technical development but the project overall? 

{ audio clip 48:30 }

Not quite sure, but a couple of things. Parallel to working on refining the AUMI in the ways that we are, we have world-class expertise on adaptive musical interfaces and their role in community formation, collective play, therapy, etc., that now we should take this knowledge and design other kinds of interfaces. 

At MCS things would often come up that inspired new interface designs. (Eric example - wheelchair sonification..) The kinds of technologies we employ in AUMI would make it easy to do. 

Would like to see us broaden the basket of the kinds of interfaces that we develop. 

[icasp]: http://www.improvcommunity.ca/
[iicsi]: http://improvisationinstitute.ca/
[keiko]: https://www.mcgill.ca/spot/keiko-shikako-thomas