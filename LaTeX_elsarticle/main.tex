%% 
%% Copyright 2007-2019 Elsevier Ltd
%% 
%% This file is part of the 'Elsarticle Bundle'.
%% ---------------------------------------------
%% 
%% It may be distributed under the conditions of the LaTeX Project Public
%% License, either version 1.2 of this license or (at your option) any
%% later version.  The latest version of this license is in
%%    http://www.latex-project.org/lppl.txt
%% and version 1.2 or later is part of all distributions of LaTeX
%% version 1999/12/01 or later.
%% 
%% The list of all files belonging to the 'Elsarticle Bundle' is
%% given in the file `manifest.txt'.
%% 
%% Template article for Elsevier's document class `elsarticle'
%% with harvard style bibliographic references

 \documentclass[preprint,12pt,review,authoryear]{elsarticle}

%% Use the option review to obtain double line spacing
% \documentclass[authoryear,preprint,review,12pt]{elsarticle}

%% Use the options 1p,twocolumn; 3p; 3p,twocolumn; 5p; or 5p,twocolumn
%% for a journal layout:
%% \documentclass[final,1p,times,authoryear]{elsarticle}
%% \documentclass[final,1p,times,twocolumn,authoryear]{elsarticle}
%% \documentclass[final,3p,times,authoryear]{elsarticle}
%% \documentclass[final,3p,times,twocolumn,authoryear]{elsarticle}
%% \documentclass[final,5p,times,authoryear]{elsarticle}
%% \documentclass[final,5p,times,twocolumn,authoryear]{elsarticle}

%% For including figures, graphicx.sty has been loaded in
%% elsarticle.cls. If you prefer to use the old commands
%% please give \usepackage{epsfig}

%% The amssymb package provides various useful mathematical symbols
\usepackage{amssymb}
%% The amsthm package provides extended theorem environments
%% \usepackage{amsthm}

%% The lineno packages adds line numbers. Start line numbering with
%% \begin{linenumbers}, end it with \end{linenumbers}. Or switch it on
%% for the whole article with \linenumbers.
\usepackage{lineno}
\usepackage{enumitem} % to set list item spacing
\usepackage{hyperref} % for embedding URLs
\usepackage{booktabs}

\journal{AUMI Book}

\begin{document}

\begin{frontmatter}

%% Title, authors and addresses

%% use the tnoteref command within \title for footnotes;
%% use the tnotetext command for theassociated footnote;
%% use the fnref command within \author or \address for footnotes;
%% use the fntext command for theassociated footnote;
%% use the corref command within \author for corresponding author footnotes;
%% use the cortext command for theassociated footnote;
%% use the ead command for the email address,
%% and the form \ead[url] for the home page:
%% \title{Title\tnoteref{label1}}
%% \tnotetext[label1]{}
%% \author{Name\corref{cor1}\fnref{label2}}
%% \ead{email address}
%% \ead[url]{home page}
%% \fntext[label2]{}
%% \cortext[cor1]{}
%% \address{Address\fnref{label3}}
%% \fntext[label3]{}

\title{AUMI Technology Development at McGill (2011 - 2019)}

%% use optional labels to link authors explicitly to addresses:
%% \author[label1,label2]{}
%% \address[label1]{}
%% \address[label2]{}

\author[1]{John Sullivan}
\author[1]{Ivan Franco}
\author[2]{Ian Hattwick}
\author[3]{Thomas Ciufo}
\author[1]{Eric Lewis}

\address[1]{McGill University, Montreal, Canada}
\address[2]{Massachusetts Institute of Technology, Cambridge, United States}
\address[3]{Mount Holyoke College, South Hadley, United States}

\begin{abstract}

    %% Text of abstract
Since its inception, AUMI has existed as both a both a high level, multifaceted project dedicated to facilitating accessible music-making, and a software Digital Musical Instrument designed to support the project's needs. The technical development of the software has passed through the hands of several developers, each of whom have provided updates and added functionality that reflected the larger contextual settings and evolving goals of the various stakeholders within the AUMI community including researchers, practitioners, teachers, and end users. This chapter provides a history of the development of the AUMI instrument with a focus on 2011 to the present, during which time the development has been housed at McGill University. The chapter is drawn from interviews with several members of the technical team. The original interviews and transcripts are available online as supplemental content to the chapter. 

% The technical development of the software has passed through the hands of several developers, each of whom have provided updates and added functionality that reflected the larger contextual settings and evolving goals of the various stakeholders within the AUMI community including researchers, practitioners, teachers, and end users. Since the release of AUMI for iOS (for tablets and smartphones), the original AUMI version for desktop has continued as a separate stand-alone application. In this chapter we reflect on how the AUMI desktop application has evolved to meet the larger goals of the project from interviews with the technical team. We highlight successes, challenges and indications for future directions of this platform, as well as the nature of technology for accessible music play and interaction. 

\end{abstract}

%%Graphical abstract
% \begin{graphicalabstract}
%\includegraphics{grabs}
% \end{graphicalabstract}

%%Research highlights
% \begin{highlights}
% \item Research highlight 1
% \item Research highlight 2
% \end{highlights}

% \begin{keyword}
%% keywords here, in the form: keyword \sep keyword

%% PACS codes here, in the form: \PACS code \sep code

%% MSC codes here, in the form: \MSC code \sep code
%% or \MSC[2008] code \sep code (2000 is the default)

% \end{keyword}

\end{frontmatter}

% \linenumbers

%% main text
\section{Introduction}
\label{sec:intro}

AUMI, the Adaptive Use Musical Instruments, can refer to a number of things: the AUMI project, an initiative conceived by project co-founders Pauline Oliveros and Leaf Miller in 2006 to facilitate inclusive music-making for people of all abilities; the AUMI consortium, a diverse group of research activities currently spread across several universities and communities in the United States and Canada; and the AUMI musical instruments, software-based \emph{digital musical instruments} (DMIs) that support the music-making endeavors of the project and consortium.

AUMI encompasses a few different instruments that have been developed at different times, by different teams, and towards different uses. In fact, it is notable that, while AUMI is commonly referenced in singular form (the Adaptive Use Musical Instrument), Pauline made a point to specify that it is the Adaptive Use \emph{Musical Instruments}, to reinforce a central theme that AUMI is more than a single technology, but a host of different approaches, ideas, and even instruments that can support participation in musicking by people of all abilities. AUMI instruments have taken a variety of forms, including a desktop application for personal computers, a separate iOS version for iPad and iPhone,\footnote{See Chapter 6 ``How Adaptive, How Useful?: Technological Design Solutions Using AUMI for iOS.''} AUMI Sings, an iOS application for inclusive choral practice,\footnote{See also Chapter 26: ``AUMI Sings: Disrupting Choral Practice.''} and a variety of prototypes, one-offs, and field tests that have come and gone throughout the many years of AUMI research. 

This chapter, along with the previous one, provides an historical account of the technical development of the AUMI desktop application, which was the original format of the AUMI digital musical instrument. The two chapters are divided into the two distinct periods of development. The previous chapter covers from 2006 to 2010 when AUMI development was carried out under the direction of Pauline Oliveros at the Deep Listening Institute (DLI). In this chapter we cover from 2011 to the present, when the technical development was housed at McGill University under the supervision of Prof. Eric Lewis. Table \ref{tab:timeline} shows a timeline of AUMI development across the two periods with the technical team and important milestones. 

\begin{table}[htbp]
    \centering
    \footnotesize
    \begin{tabular}{lllp{0.35\linewidth}}
        \toprule
        \textbf{Year(s)} &\textbf{Developer} & \textbf{Location} & \textbf{Role \& Milestones} \\
        \midrule
        % \multicolumn{3}{l}{\emph{\textbf{Deep Learning Institute and Rensselaer Polytechnic Institute}}} \\
        % \midrule
        2006 - 2007 & Zane Van Duzen & RPI & Developer, 1st prototype produced \\
        2007 - 2011 & Zevin Polzen & DLI & Developer, v1 \& 2 releases (Mac only) and updates \\
        % \midrule
        % \multicolumn{3}{l}{\emph{\textbf{McGill University}}} \\
        % \midrule
        2011 & Eric Lewis & McGill & Supervisor, McGill branch established \\
        2012 - 2014 & Ian Hattwick & McGill & Developer, updates and controller prototypes \\
        2013 - present & Thomas Ciufo & Mt. Holyoke & Liaison for technical team, onboarding for new developers \\
        \textit{2013 - present} & \textit{Henry Lowengard} & \textit{Hudson, NY} & \textit{iOS developer, 1st iOS release in 2013} \\
        2013 & Aaron Krajeski & McGill & Developer, preparation of v3 \\
        2014 & Chuck Bronson & McGill & Developer, v3 release (Mac \& Windows) \\
        2015 - 2016 & Ivan Franco &  McGill & Developer, preparation of v4 \\
        2016 - 2019 & John Sullivan & McGill & Developer, v4 release and updates \\
        \bottomrule
    \end{tabular}
    \caption{A timeline of the technical team and development milestones.}
    \label{tab:timeline}
\end{table}

A summary of the technical development during the McGill years is provided here based on interviews conducted with the following members of the technical team: 

\begin{itemize}[noitemsep]
    \item John Sullivan (interviewer) - AUMI developer (2016 - 2019) 
    \item Ivan Franco - AUMI developer (2015 - 2016)
    \item Ian Hattwick - AUMI developer (2012 - 2014)
    \item Thomas Ciufo - technical team liaison (2013 - present)
    \item Eric Lewis - supervisor, McGill chapter (2011 - present)
\end{itemize}

In the interviews we discuss how the technology has strived to reflect the overarching goals of the project, reflect on the successes and failures along the way, and consider new directions for AUMI instruments in the future. The full audio-recorded interviews and accompanying transcripts are available online as a supplement to this chapter, containing in-depth recollections and insights from the technical team.\footnote{\url{http://idmil.org/education/aumi-book/}}

\section{A Brief History of AUMI development}

\subsection{Pre-McGill}

To provide context of the AUMI instrument's move to McGill and detail of \textit{what} exactly the AUMI instrument is, we provide a short account of the early years of the project. A full history of this period is given in the previous chapter. 
% \footnote{While the project was housed at DLI, the initial AUMI prototype was built at Rensselaer Polytechnic Institute (RPI) by Zane Van Duzen, a student of Pauline's there.} 

In 2006, Leaf Miller, an occupational therapist and drummer, was leading a weekly drumming group for children and young adults with disabilities in Poughkeepsie, New York. Leaf and her longtime friend Pauline Oliveros had long recognized the strong therapeutic benefits of participation in musical activities, though group socialization and individual empowerment and self actualization of playing an instrument. However some youths were unable to participate due to their disabilities that kept them from playing a conventional instrument. Together, Leaf and Pauline conceived the idea of leveraging technology to facilitate musical interactions for these individuals, that would allow them to participate with the rest of the group. 

In 2006, Zane Van Duzen, a student at Rensselaer Polytechnic Institute (RPI) where Pauline taught, developed the first AUMI prototype,\footnote{See Chapter 2: "From Punk Philosophy to Musical Accessibility."} It ran on a Macintosh computer equipped with a web camera. Programmed in Max, a visual programming language for music and multimedia,\footnote{\url{https://cycling74.com/}} it used a software library for computer vision\footnote{\emph{cv.jit}, a package for Max by Jean-Mac Pelletier. \url{https://jmpelletier.com/cvjit/}} to track movement captured by the camera. On the screen, the individual could see the their mirror image and a tracking dot, which could be assigned to a feature to follow (e.g., a prominent body part such as the individual's hand or nose). Moving across fixed grid lines on the screen would trigger output, which could be the notes from a piano, sound effects, or other sounds saved in the program's sound library. The simple camera-based interaction worked well and a stable application was released for Macintosh computers. 

The application continued development and refinement under the direction of Zevin Polzin at the Deep Listening Institute, a center for Deep Listening practice and training created by Pauline. Version two was released providing several improvements and updates while retaining the same core functionality. 

\subsection{Transition to McGill University (2011)}

Around 2011, the Deep Listening Institute was ceasing operations in its current form.\footnote{The Deep Listening Institute eventually merged with Rensselaer Polytechnic Institute to become the Center for Deep Listening at Rensselaer. \url{https://www.deeplistening.rpi.edu/}}. With the Institute's closure, the AUMI project needed a new home which it eventually found at Rensselaer Polytechnic Institute's Center for Cognition, Communication and Culture. However the arrangement didn't include continued technical development of the AUMI software. 

Concurrently, the AUMI project was an element of research being carried out by the Improvisation, Gender, and the Body Group (IGB), which was part of a larger project entitled ``Improvisation, Community, and Social Practice'' (ICASP). The ICASP project was ongoing between several different sites, one of which was McGill University. Eric Lewis, a professor of philosophy and improvising musician was the ICASP site coordinator at McGill. Around 2011 he began to be involved with the AUMI project and in February 2012 joined the IGB group. At McGill, Eric was also involved with ongoing research at the Centre for Interdisciplinary Research in Music Media and Technology (CIRMMT) and the Input Devices and Music Interaction Laboratory (IDMIL). Given the avialable ppol of developers and resources at both CIRMMT and IDMIL, Eric proposed for AUMI development to continue under his supervision at McGill, an offer that was enthusiastically accepted by Pauline and the rest of the AUMI team. In addition to McGill's role for technical development, Eric also set up a long term pilot site for AUMI at the MacKay Centre School in Montreal, which would provide valuable feedback for the technical team to implement in the ongoing AUMI development.\footnote{See Chapter \{Ref. to Eric's chapter\}.}

\subsection{2012 - 2014}

Beginning in 2012, a steady stream of music technology graduate students from CIRMMT and IDMIL worked on AUMI development, starting with Ian Hattwick. Frequent updates were made to the current AUMI version (v2 at the time). Ongoing development focused on optimization of the user interface and other usability improvements. Special consideration was given to accessibility issues, with and small but meaningful changes that were informed by feedback from AUMI's use in the field. Deeper customization options helped to accommodate users with different abilities. For example, size and color controls for the onscreen grid lines and tracking dot provided assistance for individuals with impaired vision or colorblindness. Another update introduced a `keyboard' mode that allowed for playing diatonic scales as well as patterns of notes in sequence. 

2013 brought several changes. Thomas Ciufo, a sound artist and professor at Mount Holyoke College came to the project as a longtime friend and collaborator with Pauline. At Pauline's suggestion, he joined the project as a sort of liaison, to help bridge between the technical development and the other research activities of the team that relied on the AUMI software. An important function for Thomas has been --- and continues to be --- to provide continuity as different developers continue to work on the project. Between 2012 and 2016, AUMI development was handed over to a new student each year. Given the frequent turnover, Thomas helped to provide information and guidance as new developers came on board. 

Aaron Krajeski took over development from Ian in 2013 and worked towards the release of a major version update.\footnote{Ian remained involved with the AUMI project through 2014, though less directly involved with the release software. He conducted exploratory research to develop some proof of concept physical interfaces that could interact with the AUMI system. These experiments helped to spur interest and provide some preliminary data towards the possibility of creating bespoke devices for individuals, which is revisited in Section \ref{sec:physical-interfaces}.} Version three was finished by Chuck Bronson, who took over from Aaron in 2014. It was officially released in March 2014. As with the release of version two, it included several updates while remaining functionally similar to the original. Importantly this was the first version available for both Windows and Macintosh operating systems.

\subsubsection{AUMI for iOS}

In 2013 Henry Lowengard had prototyped and released a new version of AUMI for iOS, to run on iPad and iPhone. The iOS version is based on the same functional concept as the desktop application, in which movement captured on a device's camera is mapped to the triggering and modulation of sound output. While the desktop and iOS versions share the same DNA, they have largely continued to be developed independently and have diverged significantly in terms of features and user interface. 

The introduction of a tablet-based AUMI marked an important evolution in the way that AUMI could be deployed into various settings. Most importantly, it allowed AUMI to be much more mobile and deployable in different settings. For example, therapists, educators and other practitioners who might move between classrooms, schools or other locations can easily arrive with several iPads and quickly set them up for a group. To achieve the same mobility using the desktop application on computers would require computers to be set up and available with the AUMI software loaded at each location, or for the practitioner to carry and maintain several laptops, which would be much more expensive and require far more time to set up at each location. To aid in the rapid deployment of the tablet-based AUMI, an important feature of the iOS application was the capability to add user profiles to save and recall various user settings. This feature was not introduced in the desktop application at the time, though it would be added in a later version. 

\subsection{A brand new AUMI (2015 - 2017)}

In 2015, Ivan Franco took over development of the desktop application. While continuous updates had been made since AUMI's very first version, the interface and overall look and feel of the software hadn't been significantly altered. Similarly the application code, written in Max, had been passed from one developer to another, maintained, improved and added to, yet had not fundamentally been redrawn or reconceptualized. Rather than continue to build on top of the many layers of existing development, Ivan proposed to develop a new AUMI from the ground up, that could leverage best practices for accessibility and user interface design based on current theories and methods of Human-Computer Interaction, an interdisciplinary field or study that focuses on the design of computer technology and interactions between humans and computers. 

A preliminary version of the new AUMI desktop app was completed in 2016. It was still written in Max, chosen largely for its familiarity and widespread use amongst the music technology research community (and thus optimal for continued development by future graduate student researchers within the AUMI ecosystem). The new AUMI featured a unified user interface containing color-coded sections, organized menus, in-application help screens, along with updated tracking performance. Most importantly, Ivan's design concept was to encapsulate functionality into modules of two types. Interaction modules determine the types and directions of movements that will trigger sound events. Their output signals are mapped to sound modules which determine the types of sounds (instruments or other sound libraries), pitch or sample selections, and other audio parameters to output sound. A basic proposition for this modular approach was that it would allow for ongoing development of new modules. By adopting the same messaging protocol between the interaction and sound blocks, new functionality could easily be added and remain interoperable with the other preexisting modules.

Development was handed over to John Sullivan in the fall of 2016, and following beta testing and completion of several features, it was released as version four in 2017. One important feature that was implemented in the new version was a full system for presets that allowed for saving, recalling, importing and exporting the full application settings for individual and groups of users, based on the successful functionality of the iOS version. 

\subsection{Here and now (2018 - present)}

Since the release of version four, regular updates have continued to ensure the application is functional and runs well on both Macintosh and Windows operating systems. It has also served as a test bed of sorts, with different experimental modules being developed to test different scenarios. For example, a module was created that could utilize a haptic interface as a controller in place of the camera-based input modality.\footnote{http://idmil.org/project/max-module-for-ultrahaptics-hardware/}

As time has gone on, however, use of the desktop application has been outpaced by the iOS version, thanks in no small part to its enthusiastic and ongoing development by Henry Lowengard who has continued to introduce new and unique features, and release frequent updates. But perhaps the most vital contrast is the quick setup and ease of use that has made the iOS version imminently more practical in most real world applications. Still however, the desktop application offers several benefits and remains useful for a variety of situations. For one, it is designed to run efficiently on a wide range of personal computers, especially those running on older operating systems or with smaller memory capabilities. This makes it especially suitable for for use in a wide range of geographical and socioeconomic contexts where older computers may be more commonplace and available than iPads. 

Another strength of the desktop application is its potential for testing and experimentation, as illustrated with the haptic interface module described above. The fact that AUMI is developed in the Max programming language is especially helpful in this regard. For one, Max is a language well known amongst the academic/research/arts communities around which its development occurs. For another, the visual paradigm of Max allows new developers to easily examine and understand how the application is built, and edit or extend the application as desired. Over the years of AUMI, a number of prototypes have been developed for and with the AUMI framework. One such experiment produced an eye tracking prototype that could facilitate nonverbal communication. While commercial eye trackers may cost thousands of dollars, the prototype offered a free proof of concept device that would leverage an existing computer, web camera and customized AUMI software.

\section{Looking forward}

At the time of writing, the desktop application is at a crossroads. On one hand, there continues to be demonstrated need for a computer-based version of AUMI. On the other, there are both technical and practical hurdles to keep the desktop application viable. 

On the technical side, a primary concern comes with its development in Max. While Max provides a `low entry fee' for new developers to work on it, it is less optimized as a basis for a full-fledged computer application. Given Max's primary orientation as a language for creative audio and multimedia work, certain standard development tasks, such as compiling the AUMI software as standalone applications for both MacOS and Windows operating systems is much more difficult than with a general programming language like Python or C++.\footnote{Applications built with Max are typically run inside the Max IDE; compiling them as a standalone application is an additional and frequently problematic step.} Furthermore, regular operating system updates come with new security features, such as more sophisticated code signing and application validation measures that require reworking of certain parts of the app, especially around integration with a computer's peripherals (such as the camera and sound card) and file system access. 

On the practical side, the continued development of an AUMI desktop application is dependent on a stream of developers that will continue to maintain and extend the project. The AUMI project is shared across a consortium of several different university and community groups. Resources, personnel and funding streams, as well as research directions and needs, are ever changing. Between the success of the iOS version and limited use of the desktop application, its active development is currently paused while continuing research aims, needs and personnel are aligned. 

% \subsection{Towards a more connected and useful AUMI}

In June of 2020, members of the AUMI research community gathered for an ``AUMI Futures'' meeting. While several topics were discussed, one item was the status and future of the AUMI desktop application. While a few ideas were presented, we highlight two here that were also echoed throughout the interviews for this chapter. 

\subsection{A web-based AUMI}

A case has long been made for a version of AUMI that runs in an internet browser, and the potential benefits are many. On the development side, a web application removes some of the problematic aspects of developing and maintaining standalone apps. There is no need to create OS-specific versions, each of which require their own specific tweaks and configurations. Application is made relatively simple with several pre-existing web technologies and libraries that can handle the basic building blocks of AUMI like use of a computer's camera; synthesis, playback and manipulation of audio content, realtime video and graphic rendering in the browser, and computer vision. Potential upsides for users include the ability to run AUMI on any relatively modern, internet-connected computer, tablet or smartphone without the need to download and install an application. 

There are potential pitfalls to a browser-based AUMI as well that need to be considered and addressed. One advantage of the current platform is that it can run on relatively old computers without the need for an internet connection. Running AUMI in a browser leverages newer browser specifications for things like audio manipulation and realtime interactivity, that wouldn't be possible on an older computer or web browser. Furthermore, designing and maintaining an app like this will require individuals with web development capabilities, which moves away from the more open and approachable Max platform. 

\subsection{Physical interfaces}
\label{sec:physical-interfaces}

Another interesting prospect for future development builds on the modular approach of the current application. The AUMI application functions in a relatively simple way: a user's tracked movement is displayed on a screen superimposed with gridlines, which trigger notes when they are crossed. While this model is appropriate for a wide variety of use cases, it relies heavily on visual feedback and explicit motor control. This may be great for many, but potentially excludes other types of users, for instance those with vision impairment or extremely limited mobility. The beauty of the modular AUMI approach is that an opportunity is presented to develop different types of interfaces that could be used for different users. Taking this concept a step further, it is common for some AUMI users to use physical assistive devices already, such as wheelchairs or walkers. Given the ubiquity and relative achievability of building simple hardware interfaces with basic sensors like switches, buttons, bend sensors, accelerometers, etc., it is an enticing prospect to develop a basic architecture for a physical controller that could be quickly and easily customized for an individual AUMI user's particular needs. 

More research and experimentation is needed for these ideas to grow, however both offer interesting possibilities for continued AUMI development. 

\section{Conclusion}

At the time of writing, the AUMI project is 15 years old. AUMI, the desktop musical instrument, has changed and grown over time in many ways. However, on closer inspection, we can see that at its core, it is still a relatively simple - if ingenious - tool. The development and maintenance of the technology is but a small part of the overall project. As illustrated over and over in this book, and throughout the many overlapping examples of AUMI use and research across the several sites making up the consortium, the AUMI instruments are nothing without the people around them: the teachers, the therapists, the organizers, the researchers, and most importantly, the music-makers. 

\section*{Acknowledgements} 

The authors would like to acknowledge developers Aaron Krajeski and Chuck Bronson for their valuable contributions the AUMI project. Thank you to Marcelo Wanderley (IDMIL director, CIRMMT director 2011 - 2014) for providing the student researchers for the development team. Thanks also to Sherrie Tucker for filling gaps in our collective memories to reconstruct the history and timeline presented here. 

%% The Appendices part is started with the command \appendix;
%% appendix sections are then done as normal sections
% \appendix

% \section{Interviews}
% \label{sec:interviews}

% \include{eric_interview_transcript}

%% If you have bibdatabase file and want bibtex to generate the
%% bibitems, please use
%%
%%  \bibliographystyle{elsarticle-harv} 
%%  \bibliography{<your bibdatabase>}

%% else use the following coding to input the bibitems directly in the
%% TeX file.

% \begin{thebibliography}{00}

%% \bibitem[Author(year)]{label}
%% Text of bibliographic item

% \bibitem[ ()]{}

% \end{thebibliography}
\end{document}

\endinput
%%
%% End of file `elsarticle-template-harv.tex'.
