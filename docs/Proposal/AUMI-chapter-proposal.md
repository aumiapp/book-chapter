\pagenumbering{gobble}

An Historical Account of the AUMI Desktop Application
======================================================

Book chapter proposal by John Sullivan  
Input Devices and Music Interaction Laboratory, McGill University  
*john.sullivan2@mail.mcgill.ca*

**List of possible co-authors:** *Ivan Franco, Thomas Ciufo, Doug Van Nort, Eric Lewis, Marcelo M. Wanderley*

**Abstract:** Since its inception, AUMI - the Adaptive Use Musical Instrument - has existed as both a project dedicated to exploring specific needs and applications for accessible music making, and a software digital musical instrument designed to meet these goals. The technical development of the application has passed through the hands of several developers over the history of the project, from a first prototypes and early versions by Zane Van Duzen and Zevin Polzin beginning in 2007, to the latest version built by Ivan Franco and released in 2017, as well as the iOS version developed by Henry Lowengard. 

Conceptually and functionally, the instrument has largely remained similar to the original version, which uses a computer's built-in web camera and open source software libraries to facilitate motion-based interactions and music making for people of all abilities. However, each developer has improved and expanded the instrument in various ways that have reflected the larger contextual settings and evolving goals of the project. Development of new versions, prototypes, experiments and added functionality have been driven by input from the diverse group of stakeholders within the AUMI community comprised of researchers, practitioners, teachers, and end users.   

For this chapter we propose an historical account of the technical development of the AUMI desktop application, assembled from the point of view of members of the development team over the years. We will conduct interviews with Van Duzen and Polzin, Doug Van Nort and Ian Hattwick (and others, as available) to shed light on how the community has influenced the evolving design of early versions of AUMI, while Ivan Franco and I will bridge these historical accounts to the creation of the currently available version 4, new developments and current directions for the future of the application. Shared stories, from close collaborations with project founders Pauline Oliveros and Leaf Miller to experiences of AUMI use in a wide variety of settings, will provide a vibrant account of the evolution of the AUMI musical instrument, while elucidating the close connection between AUMI stakeholders, project goals, and ongoing technical development. 

---

**Biography:** John Sullivan is a music technology researcher and PhD candidate at McGill University. His work focuses on human-computer interaction, user-driven design of digital musical instruments, and evaluation of new technologies for music performance. He is currently the lead developer for the AUMI desktop application at the Input Devices and Music Interaction Laboratory (IDMIL). Sullivan oversaw the release of the most recent AUMI software (version 4, written by Ivan Franco) and and is currently working on new developments for future AUMI releases.