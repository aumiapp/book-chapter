# AUMI-book-chapter

Project repo for chapter contribution to the AUMI book, summer 2019. 

Proposed title: "An Historical Account of the Development of the AUMI Desktop Application"

by John Sullivan, others TBD

Due by: October 15, 2019

## Process: 

Interview several AUMI developers to track how the application has been designed and improved over time to meet the needs of the AUMI project. 

Interviews carried out in person as available, or by Skype. 

## Proposed timeline:

- Aug 23
    - Draft of interview questions
    - Contact potential interviewees
- Aug 30 
    - send out questions (for return Sept 13)
    - Schedule round table Skype for week of Sep 16 - 20? 
- Sep 16 - 20 
    - organize and notate interviews, create chapter structure
    - round table (hobefully)
- Sep 23 - 27
    - transcribe round table (get help....)
    - organize and notate round table
    - write...
- Sep 30 - Oct 4
    - write...
- Oct 7 - 11
    - write / edit
    - give to Eric, Sherrie, others for review
- *Oct 9 - 20 in Marseille for CMMR*
- Oct 15
    - submit!

## List of people to interview/include: 

1. Ian Hattwick (ian@ianhattwick.com)
2. Ivan Franco (ivan.almeida.franco@gmail.com, ivan.franco@mail.mcgill.ca)
3. Thomas Cuifo (tc@ciufo.org)
4. Eric Lewis (eric.lewis@mcgill.ca)

## Interview Questions 

**(short, for recorded interviews; taken from Sherrie's interviews)**

1. What is (or was) your involvement with the AUMI?
2. What changes in the instrument or the way it has been used have you been directly involved in? 
3. What has been the most/least effective/meaningful aspects of the AUMI for you? 
   
### Extended Interview Questions

1. How did you first learn about/get involved with AUMI?
2. Can you summarize the work that you did while you were on the project? 
3. What would you say was/were the highlight(s) of the application development while you worked on it? 
4. How, and with whom, did you interface with Pauline and the rest of the AUMI team? 
5. Were you involved in AUMI's use in the field? If so, how were you involved and did it effect the direction of the app development? 
6. What kinds of feedback did you receive feedback from the team and/or practitioners, and how did this inform development and design decisions? 
7.  How have specific technologies used in the AUMI app - for example Max and the cv.jit library for computer vision - helped (or hindered) the ongoing development of the application?
8.  Are/were there features that you would have liked to add but didn't or couldn't? What are some thoughts or ideas for the future of AUMI that you would be interested in seeing developed?
9.  Has your work with AUMI have carried over into other areas of practice, work or research?
10. Are there other aspects of your involvement with the AUMI project that you would like to share? 

--------

## Progress: 

| Interviewee | Interviewed | Transcript Draft | Transcript Revised | Notated |
| -         |:-:|:-:|:-:|:-:|
| Eric      | x | x | x | x |
| Ivan      | x | x | x | x |
| Ian       | x | x | x | x |
| Thomas    | x | x | x | x |

Next: 

- notate Ivan, Ian Thomas interviews - done
- interview notes are all coded in Nvivo now.
- transcripts: 
    - Eric and Ivan finished
    - Ian & Thomas - drafts, need to correct. 

Could write the outline and main chapter content with current Nvivo analysis, and reference full transcripts for more detail, quotes, and audio clips. 

### 11-April 2020

- Transcribed interviews are finished
- Interview notes are coded into themes in Nvivo. 
- Chapter: 
    - Intro is written
    - A Brief History is started

#### Outline: 

(Can revise this more)

1. Introduction
    1. The AUMI project and a brief history of its development at the IDMIL, especially starting with Ian. 
        1. Mention of Mackay Centre School pilot project
    2. Timeline of developers and releases/milestones
2. Version 3: improvements and developemnt
3. Version 4: redesign from the ground up
4. Current state and thoughts for future development 
5. Themes on the AUMI project - visions and high level ideas
6. Conclusion and thoughts for the future

----

New outline: 

1. Introduction
    - AUMI is many things: project, consortium, musical instrument(s)
    - What this chapter is about, and how it is put together
        - Interviewee/interviewers
        - Interview audio links / transcripts
2. A Brief History of the AUMI Desktop App
    - Timeline
        - 2006 - Leaf / Pauline
        - 2007 - 1st prototype (Zane)
        - 2007 - 2010? v1/v2/v3 (Zevin, others)
        - 2009 - Eric onboard
        - 20?? - 20?? - developers between Zevin and Ian.. who/when? 
        - 2012 - Ian developer (v3.x)
        - 2013 - Thomas Ciufo onboard - project liason
        - 2013 - iOS version released (Henry)
        - 2015 - Ivan onboard(?) (v4 dev)
        - 2016 - Johnny onboard
        - 2017 - v4 release
3. Version 3: A stable and mature AUMI
4. Version 4: A new old thing
5. Current state and thoughts for future development
6. Themes on the AUMI project - visions and high level ideas
7. Conclusion and thoughts for the future

-----

Timeline from Thomas: 

---------- Forwarded message ----------  
From: Thomas Ciufo <tciufo@mtholyoke.edu>  
Date: Apr 26, 2020, 1:22 PM -0400  
To: John Sullivan <john.sullivan2@mail.mcgill.ca>  
Cc: Thomas Ciufo <tciufo@mtholyoke.edu>  
Subject: Re: Thank you, and question about Technology Chapter

Hi Johnny,

Finally got some time to take a look at this, so here is some info that I was able to dig up.

I can’t help with the early days, since I just joined the team in summer of 2013. I had know and worked with Pauline in the past, but I met the AUMI folks at a Deep Listening conference at RPI in July of 2013. I wrote to Pauline later that summer to see how I might be able to help.

When I started that summer, Aaron Krajeski was working on the version 3. I think the Mac version 3 was already out but the PC version had not yet been finished, so that was our first goal. I think Aaron had taken over from Ian Hattwick, but I am not sure - I only interacted with Ian a little and It was some time later in the process.

Chuck Bronson chuck.james.bronson@gmail.com took over from Aaron in Jan of 2014 - Chuck did an MA thesis in Music Technology at McGill.
I believe the official launch of the version 3 on both Mac and pc as actually version 3.1 launched in April of 2014

Looks like Ivan first got involved in Jan of 2015 and started working on version 4. 

I guess you started in early 2016 - first email I have shows feb 26th but I think you had already been working with Ivan at that point?

I think version 4.1 went to Henry for app signing around late July 2017 and came back from Henry in Aug. I think we had a bit of a stall since the new web site was still in progress. I show the build dates on version 4.1 as 10/2017 - I did not try and retrace the timeline of the windows bug, since you should have those emails as well. I show a newer PC version (4.1.2) dated 7/22/2019

I think that gets us up to the present? 

So, I hope this helps and please let me know if there is other info I can help with.

All the best,
Thomas


-----

### Updated notes about developers, for timeline

- Eric Lewis joined Improvisation, Gender and the Body group in February 2012. (Sherrie email)
- Ian hired in 2012



#### My timeline: 

- 2006: Zane develops first prototype (RPI)
- 2007 - 2010: Zevin Polzin (DLI)
- 2011 - Eric onboard ICASP Gender/Body Group, development moves to McGill
- 2012 - Ian Hattwick hired
- 2013 - Aaron Krajeski takes over from Ian 
- 2014 - Chuck Bronson takes over from Aaron, v3 released
  - March 2014 - V3.0 released for Windows and Mac
<!-- - 2014? - v3.1 released? Windows support?  -->
    - 2014 - Ian working on prototyping physical interfaces
- 2015 - Ivan takes over from Chuck
  - 2015/16 - Development of v4
- 2016 - Johnny takes over from Ivan
- Oct. 2017 - AUMI v4 released
- 2019 - IICSI funding expires, Johnny leaves, development on hiatus

#### Sherrie's timeline: 

I am attaching the Annual reports from 2013-2020, as I think the form is pretty easy to follow. The names of developers appear there and some tech clues.
My guess as to the timeline is this:

- 2006-2007 Zane (dev. prototype in his senior year at RPI as Pauline's research assistant)
- 2007-2010 Zevin (lead developer, based at DLI)
- 2011-2012 Eric joins the group, hires Ian Hattwick "to work on improving AUMI software" 
- 2012-2013 Ian Hattwick and Aaron Krajeski (both CRMMT students) get funding to work on AUMI. 
- Feb. 2013: Pauline does an ICOS (?) AUMI presentation (at RPI?) trying to get interest from programmers to work on the project.
- March 2013: Eric hires Aaron Krajeski (I think Ian continues to work as well)
- By March 2014, Chuck Bronson is lead developer. (see meeting excerpts below)
- September 2014: Ian Hattwick wanting to do more developing and Eric seeking funding (see meeting minute excerpts below) 
- August 2015: Ian Hattwick and Ivan Franco 