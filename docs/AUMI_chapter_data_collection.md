# An Historical Account of the Development of the AUMI Desktop Application

This document contains resources and schedule for the proposed AUMI book chapter on the development of the desktop application.

by John Sullivan, others TBD

Due by: October 15, 2019

## General plan: 

1. Interview previous developers by email. 
2. Possible round table discussion via Skype or similar. 
3. Assemble chapter, with supplemental audio/video of round table if appropriate. 

## Schedule:

- Aug 23
    - Draft of interview questions
    - Contact potential interviewees
- Aug 30 
    - send out questions (for return Sept 13)
    - Schedule round table Skype for week of Sep 16 - 20? 
- Sep 16 - 20 
    - organize and notate interviews, create chapter structure
    - round table (hobefully)
- Sep 23 - 27
    - transcribe round table (get help....)
    - organize and notate round table
    - write...
- Sep 30 - Oct 4
    - write...
- Oct 7 - 11
    - write / edit
    - give to Eric, Sherrie, others for review
- *Oct 9 - 20 in Marseille for CMMR*
- Oct 15
    - submit!

## List of people to interview/include: 

1. Zevin Polzin (zevin@zevinpolzin.com)
2. Doug Van Nort (vannort@yorku.ca)
3. Aaron Krajeski/Chuck Bronson? (need contact info)
4. Joe Malloch? (joseph.malloch@dal.ca)
5. Ian Hattwick (ian@ianhattwick.com)
6. Ivan Franco (ivan.almeida.franco@gmail.com, ivan.franco@mail.mcgill.ca)
7. Thomas Cuifo (tc@ciufo.org)
8. Eric Lewis (eric.lewis@mcgill.ca)
9. Leaf Miller (leaf@hvc.rr.com)
- (not including Zane as he is contributing his own chapter)

## Interview Questions (long, for email)

1. How did you first learn about/get involved with AUMI?
2. Can you summarize the work that you did while you were on the project? 
3. What would you say was/were the highlight(s) of the application development while you worked on it? 
4. How, and with whom, did you interface with Pauline and the rest of the AUMI team? 
5. Were you involved in AUMI's use in the field? If so, how were you involved and did it effect the direction of the app development? 
6. What kinds of feedback did you receive feedback from the team and/or practitioners, and how did this inform development and design decisions? 
7. How have specific technologies used in the AUMI app - for example Max and the cv.jit library for computer vision - helped (or hindered) the ongoing development of the application?
8. Are/were there features that you would have liked to add but didn't or couldn't? What are some thoughts or ideas for the future of AUMI that you would be interested in seeing developed?
9. Has your work with AUMI have carried over into other areas of practice, work or research?
10. Are there other aspects of your involvement with the AUMI project that you would like to share? 

## Interview Questions 

**(short, for recorded interviews; taken from Sherrie's interviews)**

1. What is (or was) your involvement with the AUMI?
2. What changes in the instrument or the way it has been used have you been directly involved in? 
3. What has been the most/least effective/meaningful aspects of the AUMI for you? 