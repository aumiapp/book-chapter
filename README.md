# AUMI-book-chapter

Project repo for chapter contribution to the AUMI book, summer 2019. 

Working title: "An Historical Account of the Development of the AUMI Desktop Application"

by John Sullivan, others TBD

----

# Repo structure:

- docs
    - Chapter proposal
    - Misc notes
- interviews
    - notes: taken from audio recordings (before transcripts)
    - nvivo: text transcriptions (.docx), formatted for export to Nvivo qualitative analysis software
    - transcripts: text transcripts of the audio interviews
        - drafts: originals from KU
        - finished: revised/corrected by Johnny (.md, and .html for publishing online)
- **LaTeX_elsarticle**
    - LaTeX files for chapter text. 
    - working file is `main.tex`
    - output file is `main.pdf`
- media
    - interviewer/interviewee pictures (for online)
- `AUMI-dev-interviews.nvpx`: Qualitative analysis (codes and themes from interviews) conducted with Nvivo qualitative analysis software
- `README.md`: this doc.

# Online resources

The audio-recorded interviews and text transcripts are online at http://idmil.org/education/aumi-book/. 

The finished audio files are in the possession of John Sullivan and can be sent to whoever needs them. 

----

# Process: 

Interview several AUMI developers to track how the application has been designed and improved over time to meet the needs of the AUMI project. 

## List of people to interview/include: 

1. Ian Hattwick (ian@ianhattwick.com) (Skype)
2. Ivan Franco (ivan.almeida.franco@gmail.com, ivan.franco@mail.mcgill.ca)
3. Thomas Cuifo (tc@ciufo.org) (Skype)
4. Eric Lewis (eric.lewis@mcgill.ca)

## Interview Questions 

**(short, for recorded interviews; taken from Sherrie's interviews)**

1. What is (or was) your involvement with the AUMI?
2. What changes in the instrument or the way it has been used have you been directly involved in? 
3. What has been the most/least effective/meaningful aspects of the AUMI for you? 
   
### Extended Interview Questions

1. How did you first learn about/get involved with AUMI?
2. Can you summarize the work that you did while you were on the project? 
3. What would you say was/were the highlight(s) of the application development while you worked on it? 
4. How, and with whom, did you interface with Pauline and the rest of the AUMI team? 
5. Were you involved in AUMI's use in the field? If so, how were you involved and did it effect the direction of the app development? 
6. What kinds of feedback did you receive feedback from the team and/or practitioners, and how did this inform development and design decisions? 
7.  How have specific technologies used in the AUMI app - for example Max and the cv.jit library for computer vision - helped (or hindered) the ongoing development of the application?
8.  Are/were there features that you would have liked to add but didn't or couldn't? What are some thoughts or ideas for the future of AUMI that you would be interested in seeing developed?
9.  Has your work with AUMI have carried over into other areas of practice, work or research?
10. Are there other aspects of your involvement with the AUMI project that you would like to share? 

--------

## outline: 

1. Introduction
    - AUMI is many things: project, consortium, musical instrument(s)
    - What this chapter is about, and how it is put together
        - Interviewee/interviewers
        - Interview audio links / transcripts
2. A Brief History of the AUMI Desktop App
    - Timeline
        - 2006 - Leaf / Pauline
        - 2007 - 1st prototype (Zane)
        - 2007 - 2010? v1/v2/v3 (Zevin, others)
        - 2009 - Eric onboard
        - 20?? - 20?? - developers between Zevin and Ian.. who/when? 
        - 2012 - Ian developer (v3.x)
        - 2013 - Thomas Ciufo onboard - project liason
        - 2013 - iOS version released (Henry)
        - 2015 - Ivan onboard(?) (v4 dev)
        - 2016 - Johnny onboard
        - 2017 - v4 release
3. Version 3: A stable and mature AUMI
4. Version 4: A new old thing
5. Current state and thoughts for future development
6. Themes on the AUMI project - visions and high level ideas
7. Conclusion and thoughts for the future

